package com.acme.myapplication;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.acme.myapplication.Constants.Constants;
import com.acme.myapplication.Fragments.BeatComparator;
import com.acme.myapplication.Fragments.TapToStart;
import com.beatgridmedia.mobilesync.MobileSync;
import com.beatgridmedia.mobilesync.MobileSyncContext;
import com.beatgridmedia.mobilesync.MobileSyncException;
import com.beatgridmedia.mobilesync.MobileSyncMatch;
import com.beatgridmedia.mobilesync.MobileSyncMatcherTask;
import com.beatgridmedia.mobilesync.MobileSyncMedia;
import com.beatgridmedia.mobilesync.MobileSyncRuntime;
import com.beatgridmedia.mobilesync.MobileSyncSession;
import com.beatgridmedia.mobilesync.MobileSyncTask;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Properties;

import static java.lang.String.format;

public class MainActivity extends FragmentActivity {
    private static final String TAG = "MobileSync";

    private Callbacks callback;

    int mediaCount = 0, mediaProgress = 0;

    private ProgressBar progressBar;
    private Field[] drawables;
    private String imageShowing = "";
    private FrameLayout relMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);


        inItParams();

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            AppController.getInstance().object = new MobileSyncContext(getApplicationContext());
            if (null != MobileSync.getRuntime() && MobileSync.getRuntime().isConfigured()) {
                AppController.getInstance().session = AppController.getInstance().runtime.createSession();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                TapToStart matcher = new TapToStart();

                transaction.replace(R.id.container, matcher, "TapToStart");
                transaction.addToBackStack("TapToStart");
                transaction.commit();

            }

        } catch (IllegalStateException e) {
            String message = e.getMessage();
            try {
                MobileSync.onCreate(AppController.getInstance().object);
                AppController.getInstance().runtime = MobileSync.getRuntime();
                AppController.getInstance().runtime.loadConfiguration(getResources().openRawResource(R.raw.license));

            } catch (Exception e1) {
                e1.printStackTrace();
                AppController.getInstance().runtime = MobileSync.getRuntime();
                AppController.getInstance().runtime.loadConfiguration(getResources().openRawResource(R.raw.license));
            }

        } catch (Exception e) {
            AppController.getInstance().runtime = MobileSync.getRuntime();

        }
        callback = new Callbacks();
        AppController.getInstance().runtime.registerRuntimeCallback(callback);

    }

    private void inItParams() {
        relMain = (FrameLayout) findViewById(R.id.container);
        drawables = R.drawable.class.getDeclaredFields();

    }

    private class Callbacks
            extends MobileSyncSession.SessionCallbacks
            implements MobileSyncRuntime.RuntimeCallback, MobileSyncTask.TaskCallback, MobileSyncMatcherTask.MatcherCallback {


        @Override
        public void onMediaRead(MobileSyncSession session, MobileSyncMedia media) {
            super.onMediaRead(session, media);
            mediaCount++;

        }

        @Override
        public void onMediaReadingFailed(MobileSyncSession session, MobileSyncException e) {
            Log.e(TAG, "Failed to open fingerprint file.", e);

        }

        @Override
        public void onMediaReadingFinished(MobileSyncSession session) {
            super.onMediaReadingFinished(session);
            try {
                String profile = AppController.getInstance().properties.getProperty("license.profile", "speed");
                session.registerMedia(getAssets().open(format("panel-sync.car", profile)));
                progressBar.setVisibility(View.VISIBLE);
            } catch (IOException e) {
                Log.e(TAG, "Failed to open fingerprint file.", e);
            }

        }

        @Override
        public void onLoadConfigurationFinished(MobileSyncRuntime runtime, Properties properties) {

            Log.i(TAG, "MobileSync SDK Configured.");
            AppController.getInstance().session = runtime.createSession(this);
            AppController.getInstance().properties = properties;
            try {
                AppController.getInstance().session.readMedia(getAssets().open(format("panel-sync.car")));
            } catch (IOException e) {
                Log.e(TAG, "Failed to open fingerprint file.", e);
            }
        }

        @Override
        public void onLoadConfigurationFailed(MobileSyncRuntime runtime, MobileSyncException e) {
            Log.e(TAG, "Failed to configure MobileSync SDK.", e);
            Constants.displayMessage(MainActivity.this, "License validation failed", "Please check your network settings and restart App.");
        }

        @Override
        public void onMediaRegistered(MobileSyncSession session, MobileSyncMedia media) {
            mediaProgress++;

            float percentage = (mediaProgress * 100) / (mediaCount);
            progressBar.setProgress((int) percentage);


            Log.i(TAG, "Registered reference: " + media.getReference() +
                    " info: " + media.getInfo());
        }

        @Override
        public void onMediaRegistrationFinished(MobileSyncSession session) {
            progressBar.setVisibility(View.GONE);
            relMain.setBackgroundResource(R.drawable.blurred);

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            TapToStart matcher = new TapToStart();

            transaction.replace(R.id.container, matcher, "TapToStart");
            transaction.addToBackStack("TapToStart");
            transaction.commit();

        }

        @Override
        public void onMediaRegistrationFailed(MobileSyncSession session,
                                              MobileSyncException e) {
            Log.e(TAG, "Media registration failed.", e);
        }

        @Override
        public void onStateChange(MobileSyncTask task, MobileSyncTask.State state) {
            Log.d(TAG, "Task state change: " + state);
            AppController.getInstance().taskSync = task;

        }

        @Override
        public void onMatch(MobileSyncMatcherTask task, MobileSyncMatch match) {
            AppController.getInstance().task = task;

            Log.i(TAG, "Match: " + match.getMedia().getReference() + " info: " + match.getMedia().getInfo());
            Properties property = match.getMedia().getInfo();
            if (null != property.get("image") && (!imageShowing.equalsIgnoreCase((String) property.get("image")))) {
                String imageName = (String) property.get("image");
                imageName = imageName.substring(0, imageName.lastIndexOf('.'));
                imageName = imageName.replaceAll("[^A-Za-z0-9_]", "_");

                for (int i = 0; i < drawables.length; i++) {
                    String localName = drawables[i].getName();
                    if (localName.equalsIgnoreCase(imageName)) {

                        imageShowing = (String) property.get("image");
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("media_property", property);
                        bundle.putString("name", localName);

                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        BeatComparator matcher = new BeatComparator();
                        matcher.setArguments(bundle);

                        transaction.replace(R.id.container, matcher, "BeatComparator");
                        transaction.addToBackStack("BeatComparator");
                        transaction.commit();


                        break;

                    }
                }


            } else {

            }

        }


    }

    @Override
    public void onBackPressed() {

        if (getFragmentManager().getBackStackEntryCount() > 0) {
            int count = getFragmentManager().getBackStackEntryCount();
            FragmentManager.BackStackEntry stackEntry = getFragmentManager().getBackStackEntryAt(count - 1);
            String name = stackEntry.getName();
            if (null != name && (name.equalsIgnoreCase("TapToStart") || name.equalsIgnoreCase("BeatComparator"))) {
                getFragmentManager().executePendingTransactions();
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                int pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
                System.exit(0);
            } else {
                getFragmentManager().popBackStack();
            }
        } else {
            super.onBackPressed();

        }
    }

    @Override
    protected void onPause() {
        try {


            try {

                Iterable<MobileSyncTask> ww = AppController.getInstance().session.getTasks();

                for (MobileSyncTask ss : ww) {
                    ss.stop();
                }
                if (AppController.getInstance().session != null) {


                    for (int i = getFragmentManager().getBackStackEntryCount() - 1; i >= 0; i--) {
                        FragmentManager.BackStackEntry stackEntry = getFragmentManager().getBackStackEntryAt(i);

                        String name = stackEntry.getName();
                        if (null != name && (name.equalsIgnoreCase("TapToStart") || name.equalsIgnoreCase("BeatComparator"))) {
                            getFragmentManager().popBackStack();
                        }


                    }
                }

            } catch (Exception e) {

//                MobileSync.onDestroy();

                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();

    }


}
