package com.acme.myapplication.Fragments;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.acme.myapplication.R;

import java.lang.reflect.Field;
import java.util.Properties;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvertisementQuestions extends Fragment implements View.OnClickListener {

    private View view;
    private TextView txtQuestion;
    private TextView txtFlipper;
    private Properties property;
    private LinearLayout layOptions;
//    private RelativeLayout relQuiz;
    private boolean isFirstTime=false;

    public AdvertisementQuestions() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_questions_view, container, false);
        Bundle bundle = getArguments();
        property = (Properties) bundle.getSerializable("media_property");

        inItUi();
        setData();
        setlisteners();
        if(isFirstTime)
        {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    layOptions.setAlpha(1.0f);
                    txtQuestion.setAlpha(1.0f);


                }
            },600);

        }
        else {
            isFirstTime=true;
            layOptions.setAlpha(1.0f);
            txtQuestion.setAlpha(1.0f);
        }


        return view;
    }

    private void setlisteners() {
        txtFlipper.setOnClickListener(this);

    }

    private void setData() {
        if (null != property) {
            if (property.containsKey("poll.question")) {
                txtQuestion.setText((CharSequence) property.get("poll.question"));
            }
            if (property.containsKey("poll.answer1")) {
                cteateTextView("A)\t" + property.get("poll.answer1"));
            }
            if (property.containsKey("poll.answer2")) {
                cteateTextView("B)\t" + property.get("poll.answer2"));
            }
            if (property.containsKey("poll.answer3")) {
                cteateTextView("C)\t" + property.get("poll.answer3"));
            }
            if (property.containsKey("poll.answer4")) {
                cteateTextView("D)\t" + property.get("poll.answer4"));
            }
            if (property.containsKey("poll.answer5")) {
                cteateTextView("E)\t" + property.get("poll.answer5"));
            }

        }


    }

    public void cteateTextView(String text) {
        LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        parms.setMargins(0, (int) getActivity().getResources().getDimension(R.dimen._20ROR), 0, 0);
        TextView txtView = new TextView(getActivity());
        txtView.setLayoutParams(parms);
        txtView.setGravity(Gravity.LEFT);
        txtView.setTextColor(Color.WHITE);
        txtView.setText(("\t" + text));
        txtView.setTextSize(getActivity().getResources().getDimension(R.dimen._13ROR));
        layOptions.addView(txtView);


    }

    private void inItUi() {
        txtQuestion = (TextView) view.findViewById(R.id.txt_question);
        txtFlipper = (TextView) view.findViewById(R.id.txt_flipper);
        layOptions = (LinearLayout) view.findViewById(R.id.rel_option_layout);
//        relQuiz = (RelativeLayout) view.findViewById(R.id.rel_quiz);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_flipper:
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                AdvertisementProgress questionsView = new AdvertisementProgress();
                Bundle bundle = new Bundle();
                bundle.putSerializable("media_property", property);
                questionsView.setArguments(bundle);
                transaction.setCustomAnimations(
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out);

                transaction.replace(R.id.container, questionsView, "AdvertisementQuestions");
                transaction.addToBackStack(null);
                transaction.commit();

                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onPause() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                layOptions.setAlpha(0.0f);
                txtQuestion.setAlpha(0.0f);


            }
        },500);


        super.onPause();

    }
}
