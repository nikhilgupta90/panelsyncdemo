package com.acme.myapplication.Fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acme.myapplication.AppController;
import com.acme.myapplication.R;
import com.beatgridmedia.mobilesync.MobileSyncTask;

import java.lang.reflect.Field;

public class TapToStart extends Fragment implements View.OnClickListener {


    private static final String TAG = "MobileSync";

    public View view;
    LinearLayout relBackground;
    private ImageView imgBeat;
    private TextView txtSteps;

    private Handler handler;
    int i = 2;
    boolean isRunning = false;

    public TapToStart() {
    }


    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            int drawableResourceId = getResources().getIdentifier("beatgrid" + i, "drawable", getActivity().getPackageName());
            imgBeat.setImageResource(drawableResourceId);
            if (i == 24) {
                i = 0;

            }

            i++;
            handler.postDelayed(runnable, 75);


        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_start_matcher, container, false);


        inItUi();
        inItWidgetsListeners();
        handler = new Handler();
        Log.i("MobileSync", "Media registration finished.");

        return view;
    }


    private void inItWidgetsListeners() {
        imgBeat.setOnClickListener(this);
    }

    private void inItUi() {
        relBackground = (LinearLayout) view.findViewById(R.id.imgBack);
        txtSteps = (TextView) view.findViewById(R.id.txt_tap);
        imgBeat = (ImageView) view.findViewById(R.id.img_beat);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_beat:
                if (isRunning) {
                    Iterable<MobileSyncTask> ww = AppController.getInstance().session.getTasks();

                    for (MobileSyncTask ss : ww) {
                        ss.stop();
                    }
                    txtSteps.setText(R.string.tap_to_identify);
                    isRunning = false;

                    handler.removeCallbacks(runnable);
                } else {
                    handler.postDelayed(runnable, 0);
                    isRunning = true;

                    AppController.getInstance().task = AppController.getInstance().session.createMatcherTask();
                    AppController.getInstance().task.resume();
                    txtSteps.setText(R.string.tap_to_stop);
                }


                break;
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
