package com.acme.myapplication.Fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.acme.myapplication.R;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Properties;
import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvertisementProgress extends Fragment implements View.OnClickListener {

    private View view;
    private TextView txtOption1;
    private TextView txtOption2;
    private TextView txtOption3;
    private TextView txtOption4;
    private TextView txtOption5;
    private TextView txtQuestion;
    private TextView txtFlipper;
    private Properties property;

    private LinearLayout layOptions;

    private LinearLayout layOptions1;
    private LinearLayout layOptions2;
    private LinearLayout layOptions3;
    private LinearLayout layOptions4;
    private LinearLayout layOptions5;

    private ProgressBar progressBar1;
    private ProgressBar progressBar2;
    private ProgressBar progressBar3;
    private ProgressBar progressBar4;
    private ProgressBar progressBar5;
//    private RelativeLayout relQuiz;

    private HashMap<String, String> map = new HashMap<>();

    public AdvertisementProgress() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_answer_view, container, false);
        Bundle bundle = getArguments();
        property = (Properties) bundle.getSerializable("media_property");


        inItUi();
        setData();
        setlisteners();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                layOptions.setAlpha(1.0f);
                txtQuestion.setAlpha(1.0f);


            }
        }, 600);

        return view;
    }

    private void setlisteners() {
        txtFlipper.setOnClickListener(this);

    }

    private void setData() {
        if (null != property) {
            if (property.containsKey("poll.question")) {
                txtQuestion.setText((CharSequence) property.get("poll.question"));
            }
            if (property.containsKey("poll.answer1")) {
                map.put("progress1", (String) property.get("poll.question"));
            }
            if (property.containsKey("poll.answer2")) {
                map.put("progress2", (String) property.get("poll.question"));
            }
            if (property.containsKey("poll.answer3")) {
                map.put("progress3", (String) property.get("poll.question"));
            }
            if (property.containsKey("poll.answer4")) {
                map.put("progress4", (String) property.get("poll.question"));
            }
            if (property.containsKey("poll.answer5")) {
                map.put("progress5", (String) property.get("poll.question"));
            }

            setProgress();

        }


    }

    private void setProgress() {
        int totalScore = 100;
        for (int i = 0; i < map.size(); i++) {
            if (i < map.size()) {
                Random randomGenerator = new Random();

                int randomInt = randomGenerator.nextInt(100);

                int score = (i == (map.size() - 1) ? totalScore : randomInt % ((int) (0.6 * totalScore)));
                totalScore -= score;
                String txtpercent = (i + 1) + ")" + '\t' + score + "%";
                int progress = score;

                switch (i) {
                    case 0:
                        progressBar1.setProgress(progress);
                        txtOption1.setText(txtpercent);
                        layOptions1.setVisibility(View.VISIBLE);
                        txtOption1.setTextSize(getActivity().getResources().getDimension(R.dimen._13ROR));

                        break;
                    case 1:
                        progressBar2.setProgress(progress);
                        txtOption2.setText(txtpercent);
                        layOptions2.setVisibility(View.VISIBLE);
                        txtOption2.setTextSize(getActivity().getResources().getDimension(R.dimen._13ROR));

                        break;
                    case 2:
                        progressBar3.setProgress(progress);
                        txtOption3.setText(txtpercent);
                        layOptions3.setVisibility(View.VISIBLE);
                        txtOption3.setTextSize(getActivity().getResources().getDimension(R.dimen._13ROR));


                        break;
                    case 3:
                        progressBar4.setProgress(progress);
                        txtOption4.setText(txtpercent);
                        layOptions4.setVisibility(View.VISIBLE);
                        txtOption4.setTextSize(getActivity().getResources().getDimension(R.dimen._13ROR));

                        break;
                    case 4:
                        progressBar5.setProgress(progress);
                        txtOption5.setText(txtpercent);
                        layOptions5.setVisibility(View.VISIBLE);
                        txtOption5.setTextSize(getActivity().getResources().getDimension(R.dimen._13ROR));

                        break;
                }

            }

        }

    }


    private void inItUi() {
        txtQuestion = (TextView) view.findViewById(R.id.txt_question);
        txtOption1 = (TextView) view.findViewById(R.id.txt_option_1);
        txtOption2 = (TextView) view.findViewById(R.id.txt_option_2);
        txtOption3 = (TextView) view.findViewById(R.id.txt_option_3);
        txtOption4 = (TextView) view.findViewById(R.id.txt_option_4);
        txtOption5 = (TextView) view.findViewById(R.id.txt_option_5);
        txtFlipper = (TextView) view.findViewById(R.id.txt_flipper);

        layOptions = (LinearLayout) view.findViewById(R.id.rel_option_layout);
//        relQuiz = (RelativeLayout) view.findViewById(R.id.rel_quiz);

        layOptions1 = (LinearLayout) view.findViewById(R.id.layProgress1);
        layOptions2 = (LinearLayout) view.findViewById(R.id.layProgress2);
        layOptions3 = (LinearLayout) view.findViewById(R.id.layProgress3);
        layOptions4 = (LinearLayout) view.findViewById(R.id.layProgress4);
        layOptions5 = (LinearLayout) view.findViewById(R.id.layProgress5);

        progressBar1 = (ProgressBar) view.findViewById(R.id.progressBar1);
        progressBar2 = (ProgressBar) view.findViewById(R.id.progressBar2);
        progressBar3 = (ProgressBar) view.findViewById(R.id.progressBar3);
        progressBar4 = (ProgressBar) view.findViewById(R.id.progressBar4);
        progressBar5 = (ProgressBar) view.findViewById(R.id.progressBar5);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_flipper:
                getFragmentManager().popBackStack();

                break;
        }
    }

    @Override
    public void onPause() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                layOptions.setAlpha(0.0f);
                txtQuestion.setAlpha(0.0f);


            }
        },500);
        super.onPause();

    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }


}
