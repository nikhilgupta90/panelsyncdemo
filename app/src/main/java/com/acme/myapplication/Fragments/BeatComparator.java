package com.acme.myapplication.Fragments;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.renderscript.RSRuntimeException;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.acme.myapplication.R;
import com.acme.myapplication.Utils.internal.FastBlur;
import com.acme.myapplication.Utils.internal.RSBlur;

import java.lang.reflect.Field;
import java.util.Properties;

public class BeatComparator extends Fragment implements View.OnClickListener {

    private View view;
    private TextView txtTitle;
    private TextView txtSubTitle;
    private ImageView imgAdvertise;
    Properties property;
    private String path;
    private ImageView imageBack;
    private FrameLayout lay;
    private int drawableResourceId;

    public BeatComparator() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_display_advertisement, container, false);

        lay = (FrameLayout) container.findViewById(R.id.container);
        if (savedInstanceState == null) {
            Bundle bundle = getArguments();
            path = bundle.getString("name");
            property = (Properties) bundle.getSerializable("media_property");
        }


        inItUi();
        setListener();
        setData();
        return view;
    }

    private void setListener() {
        imgAdvertise.setOnClickListener(this);
    }

    private void setData() {


        Display mDisplay = getActivity().getWindowManager().getDefaultDisplay();

        Drawable d = getImage(getActivity(), path);
        if (d != null) {
//            transform(drawableToBitmap(d));
//            Picasso.with(getActivity())
//                    .load(getImageId(getActivity(), path))
//                    .transform(new BlurTransformation(getActivity(), 100, 1))
//                    .into((Target) lay);
//            Drawable dd = imageBack.getDrawable();
            imgAdvertise.setImageDrawable(d);
            drawableResourceId = getResources().getIdentifier(path, "drawable", getActivity().getPackageName());


            Drawable bluredImage=transform(BitmapFactory.decodeResource(getResources(), drawableResourceId), getActivity());
            lay.setBackgroundDrawable(bluredImage);

        }
        if (null != property) {
            if (property.containsKey("title")) {
                txtTitle.setText((CharSequence) property.get("title"));
            }
            if (property.containsKey("sub-title")) {
                txtSubTitle.setText((CharSequence) property.get("sub-title"));
            }

        }

    }

    private void inItUi() {
        txtTitle = (TextView) view.findViewById(R.id.txt_title);
        txtSubTitle = (TextView) view.findViewById(R.id.txt_sub_title);
        imgAdvertise = (ImageView) view.findViewById(R.id.img_advertisement);
        imageBack = (ImageView) view.findViewById(R.id.imageView);
    }

    public Drawable getImage(Context context, String name) {
        return context.getResources().getDrawable(context.getResources().getIdentifier(name, "drawable", context.getPackageName()));
    }

    public int getImageId(Context context, String name) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_advertisement:

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                AdvertisementQuestions questionsView = new AdvertisementQuestions();
                Bundle bundle = new Bundle();
                bundle.putSerializable("media_property", property);
                bundle.putInt("drawableResourceId", drawableResourceId);
                questionsView.setArguments(bundle);
                transaction.replace(R.id.container, questionsView, "AdvertisementQuestions");
                transaction.addToBackStack(null);
                transaction.commit();

                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static BitmapDrawable transform(Bitmap source, Context context) {
        int mRadius = 100;
        int mSampling = 1;

        int scaledWidth = source.getWidth() / mSampling;
        int scaledHeight = source.getHeight() / mSampling;

        Bitmap bitmap = Bitmap.createBitmap(scaledWidth/2, scaledHeight/2, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        canvas.scale(1 / (float) mSampling, 1 / (float) mSampling);
        Paint paint = new Paint();
        paint.setFlags(Paint.FILTER_BITMAP_FLAG);
//        paint.setAlpha(250);

        canvas.drawBitmap(source, 0, 0, paint);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            try {
                bitmap = RSBlur.blur(context, bitmap, mRadius);
            } catch (RSRuntimeException e) {
                bitmap = FastBlur.blur(bitmap, mRadius, true);
            }
        } else {
            bitmap = FastBlur.blur(bitmap, mRadius, true);
        }

        System.gc();



        source.recycle();



        return new BitmapDrawable(context.getResources(), bitmap);


    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Display mDisplay = getActivity().getWindowManager().getDefaultDisplay();
        final int width = mDisplay.getWidth();
        final int height = mDisplay.getHeight();

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

}
