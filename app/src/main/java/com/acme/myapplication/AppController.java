package com.acme.myapplication;

import android.app.Application;
import android.content.Context;

import com.beatgridmedia.mobilesync.MobileSync;
import com.beatgridmedia.mobilesync.MobileSyncContext;
import com.beatgridmedia.mobilesync.MobileSyncMatcherTask;
import com.beatgridmedia.mobilesync.MobileSyncRuntime;
import com.beatgridmedia.mobilesync.MobileSyncSession;
import com.beatgridmedia.mobilesync.MobileSyncTask;

import java.util.Properties;

/**
 * Created by Naxtre on 5/11/2016.
 */
public class AppController extends Application {
    public Context context;
    public MobileSyncSession session;
    public MobileSyncMatcherTask task;
    public Properties properties;
    public MobileSyncRuntime runtime;
    public MobileSyncTask taskSync;
    private static AppController ourInstance = new AppController();
    public MobileSyncContext object;

    public static AppController getInstance() {
        return ourInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context=getApplicationContext();
    }
}
